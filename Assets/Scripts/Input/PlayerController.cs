using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    PlayerInputActions playerInputActions;
    Rigidbody playerRigidbody;
    Vector3 startPostion;

    [SerializeField]
    float moveSpeed = 600;
    
    float multiplySpeed = 1f;

    private void Awake()
    {
        playerInputActions = new PlayerInputActions();
        playerRigidbody = GetComponent<Rigidbody>();
        startPostion = transform.position;

        playerInputActions.Kart.ResetPosition.performed += context => resetPotions();
    }

    private void OnEnable()
    {
        playerInputActions.Enable();
    }

    private void OnDisable()
    {
        playerInputActions.Disable();
    }

    private void FixedUpdate()
    {
        if (playerInputActions.Kart.SpeedUp.IsPressed()) multiplySpeed = 2; else multiplySpeed = 1;

        Vector2 moveDirection = playerInputActions.Kart.Move.ReadValue<Vector2>();
        var rotateDitrection = playerInputActions.Kart.Rotate.ReadValue<float>();
        Move(moveDirection, rotateDitrection);
    }

    private void Move(Vector2 direction, float rotate)
    {

        Quaternion deltaRotation = Quaternion.Euler(0, rotate * Time.fixedDeltaTime, 0);
        playerRigidbody.MoveRotation(playerRigidbody.rotation * deltaRotation);
        
        playerRigidbody.velocity = Quaternion.AngleAxis(transform.rotation.eulerAngles.y, Vector3.up)* new Vector3(direction.x * moveSpeed * multiplySpeed * Time.fixedDeltaTime, 0f, direction.y * moveSpeed * multiplySpeed * Time.fixedDeltaTime);

    }

    void resetPotions()
    {
        playerRigidbody.MovePosition(startPostion);
        playerRigidbody.MoveRotation(Quaternion.identity);
    }
}
